﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JoelMalone.Logging
{

    /// <summary>
    /// Abstract log class that provides static and extension methods 
    /// for logging data with context.  In general, you should use the
    /// extension methods V(), D(), L(), W() and C() to log stuff.
    /// </summary>
    public abstract class Log
    {

        static Log _instance = new TraceLog();
        static bool _hasWritten = false;

        private static LogLevel _logLevel = LogLevel.Verbose;
        public static LogLevel LogLevel { get { return _logLevel; } set { _logLevel = value; } }

        public static void SetImplementation(Log log)
        {
            if (log == null)
                throw new ArgumentNullException();

            _instance = log;

            if (_hasWritten)
                Write(log, LogLevel.Warning, "Log has been written to before implementation was set - entries may have been missed.  You should set the log implementation earlier, if possible.");
            else
                Write(log, LogLevel.Verbose, "Log set to type {0}.", log.GetType().Name);
        }

        protected abstract void OnWrite(object context, LogLevel level, string text);

        public static void Write(object context, LogLevel level, string format, params object[] args)
        {
            if (level < LogLevel)
                return;

            if (format == null)
            {
                _instance.OnWrite(context, LogLevel.Warning, "Null format string passed to Log.Write().");
                return;
            }

            try
            {
                string text = string.Format(format, args);
                _instance.OnWrite(context, level, text);
            }
            catch (Exception e)
            {
                string errorText = string.Format("An exception occured while logging the following format string:\n{0}\nException details:{1}.", format, e.ToString());
                _instance.OnWrite(context, LogLevel.Warning, errorText);
            }
        }

    }

    public static class LogExtensions
    {

        public static void V(this object me, string format, params object[] args)
        {
            if (JoelMalone.Logging.Log.LogLevel > JoelMalone.Logging.LogLevel.Verbose)
                return;
            
            JoelMalone.Logging.Log.Write(me, JoelMalone.Logging.LogLevel.Verbose, format, args);
        }

        public static void D(this object me, string format, params object[] args)
        {
            if (JoelMalone.Logging.Log.LogLevel > JoelMalone.Logging.LogLevel.Normal)
                return;
            
            JoelMalone.Logging.Log.Write(me, JoelMalone.Logging.LogLevel.Normal, format, args);
        }

        public static void L(this object me, string format, params object[] args)
        {
            if (JoelMalone.Logging.Log.LogLevel > JoelMalone.Logging.LogLevel.Normal)
                return;
            
            JoelMalone.Logging.Log.Write(me, JoelMalone.Logging.LogLevel.Normal, format, args);
        }

        public static void W(this object me, string format, params object[] args)
        {
            if (JoelMalone.Logging.Log.LogLevel > JoelMalone.Logging.LogLevel.Warning)
                return;
            
            JoelMalone.Logging.Log.Write(me, JoelMalone.Logging.LogLevel.Warning, format, args);
        }

        public static void E(this object me, Exception exception, string format, params object[] args)
        {
            if (JoelMalone.Logging.Log.LogLevel > JoelMalone.Logging.LogLevel.Critical)
                return;

            JoelMalone.Logging.Log.Write(me, JoelMalone.Logging.LogLevel.Critical, format, args);

            var exceptionText = exception.ToString();
            JoelMalone.Logging.Log.Write(me, JoelMalone.Logging.LogLevel.Critical, exceptionText);
        }

        public static void C(this object me, string format, params object[] args)
        {
            if (JoelMalone.Logging.Log.LogLevel > JoelMalone.Logging.LogLevel.Critical)
                return;

            JoelMalone.Logging.Log.Write(me, JoelMalone.Logging.LogLevel.Critical, format, args);
        }

        public static void Log(this Exception me)
        {
            JoelMalone.Logging.Log.Write(me, JoelMalone.Logging.LogLevel.Critical, me.ToString());
        }

        public static void Log(this Exception me, string format, params object[] args)
        {
            JoelMalone.Logging.Log.Write(me, JoelMalone.Logging.LogLevel.Critical, format, args);
            JoelMalone.Logging.Log.Write(me, JoelMalone.Logging.LogLevel.Critical, me.ToString());
        }

        public static void WarnNulls(this object me, params object[] objects)
        {
            int nullCount = objects.Count(o => o == null);

            if (nullCount > 0)
                JoelMalone.Logging.Log.Write(me, JoelMalone.Logging.LogLevel.Warning, "{0} null arguments passed to method.", nullCount);
        }

    }

    class NullLog : Log
    {
        protected override void OnWrite(object context, LogLevel level, string text)
        {
        }
    }

}
