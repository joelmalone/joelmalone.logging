﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace JoelMalone.Logging
{
    public class TraceLog : Log
    {

        const string LOG_FORMAT = "[{0:yyyy/MM/dd HH:mm:ss} {1}] {2}";

        public TraceLog()
        {
            TextWriterTraceListener trl = new TextWriterTraceListener(System.Console.Out);
            Trace.Listeners.Add(trl);
        }

        protected override void OnWrite(object context, LogLevel level, string text)
        {
            var line = string.Format(LOG_FORMAT, DateTime.UtcNow, context.GetType().Name, text);
            Trace.WriteLine(line, Enum.GetName(typeof(LogLevel), level));
        }
    }
}
