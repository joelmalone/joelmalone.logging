﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JoelMalone.Logging
{

    /// <summary>
    /// The various levels of severity attributed to each log entry.
    /// </summary>
    public enum LogLevel
    {
        /// <summary>
        /// Super-spammy log level that is probably only needed during specific investigations.
        /// </summary>
        Verbose,
        /// <summary>
        /// Normal/debug log level.
        /// </summary>
        Normal,
        /// <summary>
        /// Warning log level - something odd happened, but execution will probably continue normally.
        /// </summary>
        Warning,
        /// <summary>
        /// Something unexpected has happened and execution probably won't continue normally.
        /// </summary>
        Critical,
    }

}
